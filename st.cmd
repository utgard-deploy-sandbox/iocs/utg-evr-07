#!/usr/bin/env iocsh.bash

################################################################
### requires
require(mrfioc2)
require(evr_timestamp_buffer)
require(evr_seq_calc)

epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")

#iocshLoad("./iocsh/env-init.iocsh")
iocshLoad("$(mrfioc2_DIR)/env-init.iocsh")


epicsEnvSet("IOC", "LabS-Utgard-VIP:TS")
epicsEnvSet("PCIID", "01:00.0")
epicsEnvSet("DEV", "EVR-7")
epicsEnvSet("EVR", "$(DEV)")
epicsEnvSet("CHIC_SYS", "LabS-Utgard-VIP:")
epicsEnvSet("CHOP_DRV", "Chop-Drv-01")
epicsEnvSet("CHIC_DEV", "TS-$(DEV)")
epicsEnvSet("BUFFSIZE", "100")
epicsEnvSet("MRF_HW_DB", "evr-pcie-300dc-ess.db")

# Load e3-common
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

#iocshLoad("./iocsh/evr-pcie-300dc-init.iocsh", "S=$(IOC), DEV=$(DEV), PCIID=$(PCIID)")
iocshLoad("$(mrfioc2_DIR)/evr-pcie-300dc-init.iocsh", "S=$(IOC), DEV=$(DEV), PCIID=$(PCIID)")

#mrmEvrSetupPCI("$(EVR)", $(PCI_SLOT))
#dbLoadRecords("$(MRF_HW_DB)","EVR=$(EVR),IOC=$(IOC),D=$(DEV),FEVT=88.0525,PINITSEQ=0")
# The amount of time which the EVR will wait for the 1PPS event before going into error state.
#var(evrMrmTimeNSOverflowThreshold, 1000000)

# Load timestamp buffer database
iocshLoad("$(evr_timestamp_buffer_DIR)/evr_timestamp_buffer.iocsh", "CHIC_SYS=$(CHIC_SYS), CHIC_DEV=$(CHIC_DEV), CHOP_DRV=$(CHOP_DRV), SYS=$(IOC), BUFFSIZE=$(BUFFSIZE)")

time2ntp("$(EVR)", 2)

# Add records to timestamp more events
dbLoadRecords("evrevent.db","EN=$(IOC)-$(DEV):EvtI, OBJ=$(DEV), CODE=18, EVNT=18"))
dbLoadRecords("evrevent.db","EN=$(IOC)-$(DEV):EvtJ, OBJ=$(DEV), CODE=19, EVNT=19"))
dbLoadRecords("evrevent.db","EN=$(IOC)-$(DEV):EvtK, OBJ=$(DEV), CODE=20, EVNT=20"))

iocInit()

#iocshLoad("./iocsh/evr-run.iocsh", "IOC=$(IOC), DEV=$(DEV)")
iocshLoad("$(mrfioc2_DIR)/evr-run.iocsh", "IOC=$(IOC), DEV=$(DEV)")

# Global default values
# Set the frequency that the EVR expects from the EVG for the event clock
#dbpf $(IOC)-$(DEV):Time-Clock-SP 88.0525
#dbpf $(IOC)-$(DEV):Link-Clk-SP 88.0525
#dbpf $(IOC)-$(DEV):Ena-Sel "Enabled"

# Set delay compensation target. This is required even when delay compensation
# is disabled to avoid occasionally corrupting timestamps.
#dbpf $(IOC)-$(DEV):DC-Tgt-SP 10000
#dbpf $(IOC)-$(DEV):DC-Ena-Sel "Enable"

##### Set the FIFO buffer retrieval period. Default is 0.001 #####
var mrmEvrFIFOPeriod 0.001

######### INPUTS #########
# Set up of UnivIO 0 as Input. Generate Code 10 locally on rising edge.
dbpf $(IOC)-$(DEV):In0-Lvl-Sel "Active High"
dbpf $(IOC)-$(DEV):In0-Edge-Sel "Active Rising"
dbpf $(IOC)-$(DEV):OutFPUV00-Src-SP 61
dbpf $(IOC)-$(DEV):In0-Trig-Ext-Sel "Edge"
dbpf $(IOC)-$(DEV):In0-Code-Ext-SP 20

# Set up of UnivIO 1 as Input. Generate Code 11 locally on rising edge.
dbpf $(IOC)-$(DEV):In1-Lvl-Sel "Active High"
dbpf $(IOC)-$(DEV):In1-Edge-Sel "Active Rising"
dbpf $(IOC)-$(DEV):OutFPUV01-Src-SP 61
dbpf $(IOC)-$(DEV):In1-Trig-Ext-Sel "Edge"
dbpf $(IOC)-$(DEV):In1-Code-Ext-SP 21

# Set up of UnivIO 1 as Input. Generate Code 12 locally on rising edge.
dbpf $(IOC)-$(DEV):In2-Lvl-Sel "Active High"
dbpf $(IOC)-$(DEV):In2-Edge-Sel "Active Rising"
dbpf $(IOC)-$(DEV):OutFPUV02-Src-SP 61
dbpf $(IOC)-$(DEV):In2-Trig-Ext-Sel "Edge"
dbpf $(IOC)-$(DEV):In2-Code-Ext-SP 22

# Set up of UnivIO 1 as Input. Generate Code 13 locally on rising edge.
dbpf $(IOC)-$(DEV):In3-Lvl-Sel "Active High"
dbpf $(IOC)-$(DEV):In3-Edge-Sel "Active Rising"
dbpf $(IOC)-$(DEV):OutFPUV03-Src-SP 61
dbpf $(IOC)-$(DEV):In3-Trig-Ext-Sel "Off"
dbpf $(IOC)-$(DEV):In3-Code-Ext-SP 23


# Trig-Ext-Sel changed from "Off" to "Edge", Code-Ext-SP changed from 0 to 10
dbpf $(IOC)-$(DEV):UnivIn0-Lvl-Sel "Active High"
dbpf $(IOC)-$(DEV):UnivIn0-Edge-Sel "Active Rising"
dbpf $(IOC)-$(DEV):OutFPUV00-Src-SP 61
dbpf $(IOC)-$(DEV):UnivIn0-Trig-Ext-Sel "Edge"
dbpf $(IOC)-$(DEV):UnivIn0-Trig-Back-Sel "Off"
dbpf $(IOC)-$(DEV):UnivIn0-Code-Ext-SP 20
dbpf $(IOC)-$(DEV):UnivIn0-Code-Back-SP 0

dbpf $(IOC)-$(DEV):UnivIn1-Lvl-Sel "Active High"
dbpf $(IOC)-$(DEV):UnivIn1-Edge-Sel "Active Rising"
dbpf $(IOC)-$(DEV):OutFPUV01-Src-SP 61
dbpf $(IOC)-$(DEV):UnivIn1-Trig-Ext-Sel "Edge"
dbpf $(IOC)-$(DEV):UnivIn1-Trig-Back-Sel "Off"
dbpf $(IOC)-$(DEV):UnivIn1-Code-Ext-SP 21
dbpf $(IOC)-$(DEV):UnivIn1-Code-Back-SP 0

dbpf $(IOC)-$(DEV):UnivIn2-Lvl-Sel "Active High"
dbpf $(IOC)-$(DEV):UnivIn2-Edge-Sel "Active Rising"
dbpf $(IOC)-$(DEV):OutFPUV02-Src-SP 61
dbpf $(IOC)-$(DEV):UnivIn2-Trig-Ext-Sel "Edge"
dbpf $(IOC)-$(DEV):UnivIn2-Trig-Back-Sel "Off"
dbpf $(IOC)-$(DEV):UnivIn2-Code-Ext-SP 22
dbpf $(IOC)-$(DEV):UnivIn2-Code-Back-SP 0

dbpf $(IOC)-$(DEV):UnivIn3-Lvl-Sel "Active High"
dbpf $(IOC)-$(DEV):UnivIn3-Edge-Sel "Active Rising"
dbpf $(IOC)-$(DEV):OutFPUV03-Src-SP 61
dbpf $(IOC)-$(DEV):UnivIn3-Trig-Ext-Sel "Edge"
dbpf $(IOC)-$(DEV):UnivIn3-Trig-Back-Sel "Off"
dbpf $(IOC)-$(DEV):UnivIn3-Code-Ext-SP 23
dbpf $(IOC)-$(DEV):UnivIn3-Code-Back-SP 0

dbpf $(IOC)-$(DEV):EvtA-SP.OUT "@OBJ=$(EVR),Code=10" 
dbpf $(IOC)-$(DEV):EvtA-SP.VAL 10 
dbpf $(IOC)-$(DEV):EvtB-SP.OUT "@OBJ=$(EVR),Code=11" 
dbpf $(IOC)-$(DEV):EvtB-SP.VAL 11 
dbpf $(IOC)-$(DEV):EvtC-SP.OUT "@OBJ=$(EVR),Code=12" 
dbpf $(IOC)-$(DEV):EvtC-SP.VAL 12
dbpf $(IOC)-$(DEV):EvtD-SP.OUT "@OBJ=$(EVR),Code=13"
dbpf $(IOC)-$(DEV):EvtD-SP.VAL 13
dbpf $(IOC)-$(DEV):EvtE-SP.OUT "@OBJ=$(EVR),Code=14"
dbpf $(IOC)-$(DEV):EvtE-SP.VAL 14
dbpf $(IOC)-$(DEV):EvtF-SP.OUT "@OBJ=$(EVR),Code=15"
dbpf $(IOC)-$(DEV):EvtF-SP.VAL 15
dbpf $(IOC)-$(DEV):EvtG-SP.OUT "@OBJ=$(EVR),Code=16"
dbpf $(IOC)-$(DEV):EvtG-SP.VAL 16
dbpf $(IOC)-$(DEV):EvtH-SP.OUT "@OBJ=$(EVR),Code=17"
dbpf $(IOC)-$(DEV):EvtH-SP.VAL 17


######### OUTPUTS #########
#Set up delay generator 0 to trigger on event 14
dbpf $(IOC)-$(DEV):DlyGen0-Width-SP 1000 #1ms
dbpf $(IOC)-$(DEV):DlyGen0-Delay-SP 0 #0ms
dbpf $(IOC)-$(DEV):DlyGen0-Evt-Trig0-SP 14
dbpf $(IOC)-$(DEV):OutFPUV08-Src-SP 0

#Set up delay generator 1 to trigger on event 125
dbpf $(IOC)-$(DEV):DlyGen1-Width-SP 2860 #1ms
dbpf $(IOC)-$(DEV):DlyGen1-Delay-SP 0 #0ms
dbpf $(IOC)-$(DEV):DlyGen1-Evt-Trig0-SP 125
dbpf $(IOC)-$(DEV):OutFPUV09-Src-SP 1

#Set up delay generator 2 to trigger on event 17
dbpf $(IOC)-$(DEV):DlyGen2-Width-SP 1000 #1ms
dbpf $(IOC)-$(DEV):DlyGen2-Delay-SP 0 #0ms
dbpf $(IOC)-$(DEV):DlyGen2-Evt-Trig0-SP 17
dbpf $(IOC)-$(DEV):OutFPUV04-Src-SP 2 #Connect outputx to DlyGen-y

#Set up delay generator 3 to trigger on event 18
dbpf $(IOC)-$(DEV):DlyGen3-Width-SP 1000 #1ms
dbpf $(IOC)-$(DEV):DlyGen3-Delay-SP 0 #0ms
dbpf $(IOC)-$(DEV):DlyGen3-Evt-Trig0-SP 18
dbpf $(IOC)-$(DEV):OutFPUV05-Src-SP 3 

######## Sequencer #########
#dbpf $(IOC)-$(DEV):Base-Freq 14.00000064
dbpf $(IOC)-$(DEV):End-Event-Ticks 4

# Load sequencer setup
dbpf $(IOC)-$(DEV):SoftSeq0-Load-Cmd 1

# Enable sequencer
dbpf $(IOC)-$(DEV):SoftSeq0-Enable-Cmd 1

# Select run mode, "Single" needs a new Enable-Cmd every time, "Normal" needs Enable-Cmd once
dbpf $(IOC)-$(DEV):SoftSeq0-RunMode-Sel "Normal"

# Load sequence events and corresponding tick lists
#system "/bin/bash /epics/iocs/cmds/labs-utgard-evr2/conf_evr_seq.sh"

# Use ticks or microseconds
dbpf $(IOC)-$(DEV):SoftSeq0-TsResolution-Sel "Ticks"

# Select trigger source for soft seq 0, trigger source 0, delay gen 0
dbpf $(IOC)-$(DEV):SoftSeq0-TrigSrc-0-Sel 0

# Commit all the settings for the sequnce
# commit-cmd by evrseq!!! 
epicsThreadSleep 2

dbpf $(IOC)-$(DEV):SoftSeq0-Commit-Cmd "1"
